#ifndef FLATFRAME_H
#define FLATFRAME_H

#include <QWidget>

namespace Ui {
class FlatFrame;
}

class FlatFrame : public QWidget
{
    Q_OBJECT

public:
    explicit FlatFrame(QWidget *parent = nullptr);
    ~FlatFrame();

private:
    Ui::FlatFrame *ui;
};

#endif // FLATFRAME_H
