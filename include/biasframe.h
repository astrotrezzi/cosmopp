#ifndef BIASFRAME_H
#define BIASFRAME_H

#include <QWidget>

namespace Ui {
class BiasFrame;
}

class BiasFrame : public QWidget
{
    Q_OBJECT

public:
    explicit BiasFrame(QWidget *parent = nullptr);
    ~BiasFrame();

private:
    Ui::BiasFrame *ui;
};

#endif // BIASFRAME_H
