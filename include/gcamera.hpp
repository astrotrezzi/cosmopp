#ifndef GCAMERA_HPP
#define GCAMERA_HPP

#include "include/camera.hpp"
#include <QProcess>

namespace cosmopp
{
    class GCamera : public Camera
    {
    public:
        GCamera();
        ~GCamera();

        void initCamera();

        int getCameraModel(QString& cameraName) override;
        int getBatteryStatus(int& batteryLevel) override;

        int getCameraIso(QString &iso, const bool list = false, QVector<QString>* isoList = nullptr) override;
        int getCameraExposure(QString& exposure, const bool list = false, QVector<QString>* exposureList = nullptr) override;
        int getCameraAperture(QString& aperture, const bool list = false, QVector<QString>* apertureList = nullptr) override;
        int getCameraDriveMode(QString& driveMode, const bool list = false, QVector<QString>* driveModeList = nullptr) override;
        int getCameraFocus(QString& focus, const bool list = false, QVector<QString>* focusList = nullptr) override;
        int getCameraWB(QString& wb, const bool list = false, QVector<QString>* wbList = nullptr) override;
        int getCameraColorSpace(QString& colorSpace, const bool list = false, QVector<QString>* colorSpaceList = nullptr) override;
        int getCameraImageFormat(QString& format, const bool list = false, QVector<QString>* formatList = nullptr) override;

        int setCameraIso(const QString iso) override;
        int setCameraExposure(const QString exposure) override;
        int setCameraAperture(const QString aperture) override;
        int setCameraDriveMode(const QString driveMode) override;
        int setCameraFocus(const QString focus) override;
        int setCameraWB(const QString wb) override;
        int setCameraColorSpace(const QString colorSpace) override;
        int setCameraImageFormat(const QString format) override;
        void setCaptureParameters(const QString path, const bool bulb = false, const int exposure = 0, const bool onCamera = false) override;
        void stopCaptureImage() override;

    public slots:
       int captureImage() override;

    private:
        std::vector<size_t> findAll(const QString str, const QString sub);
        int getConfig(const QString cmd, QString& output, const bool moreOptions = false, QVector<QString>* options = {});
        int setConfig(const QString cmd, const QString& input);

    private:
        QProcess m_gphotoProcess;
        QString m_path;
        bool m_bulb;
        int m_exposure;
        bool m_onCamera;
        std::atomic<bool> m_exposureInter;
        static constexpr int DOWNLOAD_DELAY_TIME = 4; //delay time used during bulb exposure (in sec)
        QString m_camerainitCmd = "killall gvfs-gphoto2-volume-monitor gvfsd-gphoto2";
        QString m_gphoto2Cmd = "gphoto2 ";
        QString m_getConfigCmd = "--get-config=";
        QString m_setConfigCmd = "--set-config";
        QString m_captureImgCmd = "--capture-image-and-download";
        QString m_forceOverwriteCmd = "--force-overwrite";
        QString m_keepCmd = "--keep";
        QString m_filenameCmd = "--filename=";
        QString m_intervalCmd = "--interval=";
        QString m_framesCmd = "--frames";
    };
}


#endif // GCAMERA_HPP
