#ifndef LIGHTFRAME_H
#define LIGHTFRAME_H

#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include "include/common.hpp"
#include "include/histo.hpp"

static constexpr int NSUBFRAMES = 5;

namespace Ui {
class LightFrame;
}

class LightFrame : public QWidget
{
    Q_OBJECT

public:
    explicit LightFrame(QWidget *parent = nullptr);
    ~LightFrame();

private slots:
    void on_newImageAcquired(const AstroImageInfo imageInfo);
    void on_updateImageHisto(const QString imagePath, const bool enableStatistics);
    void on_openFilePushButton_pressed();
    void on_wrongFrameSpinBox_valueChanged(int arg1);
    void on_clearIntPushButton_pressed();
    void on_currentImgPushButton_pressed();

    void on_clearStatsToolButton_pressed();

private:
    enum class ImageSub
    {
        IMG_CENTRAL = 0,
        IMG_TOP_LEFT,
        IMG_BOTTOM_LEFT,
        IMG_TOP_RIGHT,
        IMG_BOTTOM_RIGHT
    };

    enum class ImageQuality
    {
        IMGQ_EXCELLENT = 0,
        IMGQ_GOOD,
        IMGQ_FAIR,
        IMGQ_POOR,
        IMGQ_VERY_POOR,
        IMGQ_NOT_AVAILABLE
    };

    enum class ImageTrend
    {
        IMGT_INCREASE = 0,
        IMGT_DECREASE,
        IMGT_STABLE,
        IMGT_NOT_AVAILABLE
    };

private:
    void setImagePath(const QString imagePath) {m_imagePath = imagePath;}
    QString getImagePath() {return m_imagePath;}
    void updateHistogram(const QString imagePath);
    void updateFrames(const QString imagePath);
    QImage getImageSub(const QImage original, const int topleftX, const int topleftY, const int bottomRightX, const int bottomRightY);
    void updateStats(const bool enable);
    void getFocusQuality(ImageQuality& quality);
    void getSkyConditions(ImageQuality& quality);
    void getTrackingQuality(ImageQuality& quality);
    void getGradients(ImageQuality& quality);
    ImageTrend getExposureTrend();
    ImageTrend getHotPixels();
    void setQualityStatus(QLineEdit*& lineEdit, const ImageQuality quality);
    void setTrendStatus(QLabel*& label, const ImageTrend trend);

private:
    Ui::LightFrame *ui;
    QString m_imagePath = "";
    AstroImageInfo m_imageInfo;
    cosmopp::ImageHistogram m_imageHistogram;
    int m_frameNum = 0;
    int m_integrationTime = 0;
    QGraphicsScene m_subFrameScene[NSUBFRAMES];
    QGraphicsPixmapItem* m_subFrame[NSUBFRAMES] = {nullptr, nullptr, nullptr, nullptr, nullptr};
    std::vector<cosmopp::ImageHistogram::ImageStats> m_stats;
};

#endif // LIGHTFRAME_H
