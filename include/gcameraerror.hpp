#ifndef GCAMERAERROR_HPP
#define GCAMERAERROR_HPP

static constexpr int GCAMERA_NO_ERROR = 0x00;
static constexpr int GCAMERA_COMMUNICATION_ERROR = 0x01;
static constexpr int GCAMERA_PROTOCOL_ERROR = 0x02;

#endif // GCAMERAERROR_HPP
