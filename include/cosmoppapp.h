#ifndef COSMOPPAPP_H
#define COSMOPPAPP_H

#include <QMainWindow>
#include <QThread>
#include "include/gcamera.hpp"
#include "include/imageinfo.h"
#include "include/imagehisto.h"
#include "include/lightframe.h"
#include "include/biasframe.h"
#include "include/darkframe.h"
#include "include/flatframe.h"
#include <QGraphicsScene>
#include <QLayout>
#include <QLabel>
#include "include/common.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class CosmoppApp; }
QT_END_NAMESPACE

class CosmoppApp : public QMainWindow
{
    Q_OBJECT

public:
    CosmoppApp(QWidget *parent = nullptr);
    ~CosmoppApp();
    void showEvent(QShowEvent *event);
    void resizeEvent(QResizeEvent *event);

protected:
    //void closeEvent(QCloseEvent *event);
    bool eventFilter(QObject *obj, QEvent *ev);

signals:
    void exeSequence(int frame);
    void updateImageInfo(const QString info);
    void updateImageCrop(const QPixmap crop, const QPointF posMouse);
    void updateImageHisto(const QString imagePath, const bool enableStatistics);
    void updateImageHistoTool(const QString imagePath, const bool enableStatistics);
    void newImageAcquired(const AstroImageInfo imageInfo);

private slots:
    void on_setIsoPushButton_pressed();
    void on_setExposurePushButton_pressed();
    void on_setAperturePushButton_pressed();
    void on_setDrivemodePushButton_pressed();
    void on_setFocusPushButton_pressed();
    void on_setWbPushButton_pressed();
    void on_setColorspacePushButton_pressed();
    void on_setImgformatPushButton_pressed();
    void updateBattery();
    void on_updateFocusPushButton_pressed();
    void on_startCapturePushButton_pressed();
    void on_stopCapturePushButton_pressed();
    void on_showImageAcquired(QString path);
    void startCapture();
    void on_selectFolderButton_pressed();
    void on_draftCheckBox_stateChanged(int arg1);
    void on_cameraTabWidget_currentChanged(int index);
    void on_seqStartButton_pressed();
    void on_seqStopButton_pressed();
    void on_exeSequence(int frame);
    void on_seqCaptureThreadFinished();
    void on_imageInfoToolButton_pressed();
    void on_seqImageInfoToolButton_pressed();
    void on_histoToolButton_pressed();
    void on_seqHistoToolButton_pressed();

private:
    void initStatusBar();
    void initIso();
    void initExposure();
    void initAperture();
    void initDriveMode();
    void initFocus();
    void initWb();
    void initColorSpace();
    void initImageFormat();
    void initSequenceTab();
    void checkBattery();
    void updateImageFileInfo(const QString path);
    void delay_MSec(unsigned int msec);

private:
    Ui::CosmoppApp *ui = nullptr;
    cosmopp::GCamera* m_camera = nullptr;
    QTimer *m_batteryTimer = nullptr;
    QThread* m_captureThread = nullptr;
    static constexpr int BATTERY_LEVEL_TIMEOUT = 600000; //ms
    static constexpr int IMG_INDEX_DIGITS = 4; //number of digits of image index
    QString m_imageDir = "";
    QString m_imageFileName = "";
    QString m_imageFileFormat = "";
    QString m_imagePath = "";
    int m_imageIndex = 0;
    QGraphicsScene m_preview_scene;
    QGraphicsScene m_sequence_scene;
    QGraphicsPixmapItem* m_preview = nullptr;
    QGraphicsPixmapItem* m_sequence = nullptr;
    QWidget* m_batteryWidget = nullptr;
    QHBoxLayout* m_statusBarLayout = nullptr;
    QLabel* m_battery_level = nullptr;
    QLabel* m_battery_percentage = nullptr;
    QLabel* m_cameraModel = nullptr;
    QSpacerItem* m_spacer = nullptr;
    bool m_sequenceEnabled = false;
    ImageInfo* m_imageInfo = nullptr;
    ImageHisto* m_imageHisto = nullptr;
    QString m_currentPreviewImagePath = "";
    QString m_currentSequenceImagePath = "";
    bool m_isSequenceTab = false;
    //Light Frame Object
    LightFrame* m_lightFrame = nullptr;
    BiasFrame* m_biasFrame = nullptr;
    DarkFrame* m_darkFrame = nullptr;
    FlatFrame* m_flatFrame = nullptr;
    AstroImageType m_imageType = COSMOPP_NOINFO_FRAME;
};
#endif // COSMOPPAPP_H
