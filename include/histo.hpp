#ifndef HISTO_HPP
#define HISTO_HPP

#include <QObject>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QtCharts/QChartView>

namespace cosmopp
{
    class ImageHistogram : public QObject
    {
        Q_OBJECT

    public:
        ImageHistogram(QWidget *parent = nullptr);
        ~ImageHistogram();

        struct ImageStats
        {
            int maximumBL = 0;        //brightness level at maximum
            int saturatedPixel = 0;   //number of saturated pixel
            int maximumPixel = 0;     //number of pixel at maximum
        };

        void getImageHisto(const QString imagePath, QtCharts::QChartView *&plot, ImageHistogram::ImageStats& imageStat);
        void getImageHisto(const QString imagePath, QtCharts::QChartView *&plot);

    private:
        void createImage(const QString imagePath);
        void imageProcess(int* histo);
        void createHisto(const int* histo, QtCharts::QChartView*& plot);
        ImageStats imageStats(const int* histo);

    private:
        QImage m_imagePix;
        QGraphicsScene m_scene;
        QGraphicsPixmapItem* m_image = nullptr;
    };
}


#endif // HISTO_HPP
