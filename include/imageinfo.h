#ifndef IMAGEINFO_H
#define IMAGEINFO_H

#include <QDialog>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>

namespace Ui {
class ImageInfo;
}

class ImageInfo : public QDialog
{
    Q_OBJECT

public:
    explicit ImageInfo(QWidget *parent = nullptr);
    ~ImageInfo();

    int cropWidth() const;
    int cropHeight() const;

public slots:
    void on_updateImageInfo(const QString info);
    void on_updateImageCrop(const QPixmap crop, const QPointF posMouse);

private:
    Ui::ImageInfo *ui;
    QGraphicsScene m_scene;
    QGraphicsPixmapItem* m_pixmapItem;
};

#endif // IMAGEINFO_H
