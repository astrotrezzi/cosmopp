#ifndef COMMON_HPP
#define COMMON_HPP

#include <QString>

enum AstroImageType {
    COSMOPP_LIGHT_FRAME = 0,
    COSMOPP_DARK_FRAME,
    COSMOPP_BIAS_FRAME,
    COSMOPP_FLAT_FRAME,
    COSMOPP_NOINFO_FRAME
};

struct AstroImageInfo {
    AstroImageType imageType;
    QString exposureTime;
    QString iso;
    QString aperture;
    QString format;
    bool longExposure;
    int index;
    QString imagePath;
};

#endif // COMMON_HPP
