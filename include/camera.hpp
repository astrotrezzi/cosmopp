#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <QObject>

namespace cosmopp
{
    class Camera : public QObject
    {
        Q_OBJECT

        public:
            Camera() {};
            virtual ~Camera() {};

            virtual int getCameraModel(QString& cameraName) = 0;
            virtual int getBatteryStatus(int& batteryLevel) = 0;

            virtual int getCameraIso(QString &iso, const bool list = false, QVector<QString>* isoList = nullptr) = 0;
            virtual int getCameraExposure(QString& exposure, const bool list = false, QVector<QString>* exposureList = nullptr) = 0;
            virtual int getCameraAperture(QString& aperture, const bool list = false, QVector<QString>* apertureList = nullptr) = 0;
            virtual int getCameraDriveMode(QString& driveMode, const bool list = false, QVector<QString>* driveModeList = nullptr) = 0;
            virtual int getCameraFocus(QString& focus, const bool list = false, QVector<QString>* focusList = nullptr) = 0;
            virtual int getCameraWB(QString& wb, const bool list = false, QVector<QString>* wbList = nullptr) = 0;
            virtual int getCameraColorSpace(QString& colorSpace, const bool list = false, QVector<QString>* colorSpaceList = nullptr) = 0;
            virtual int getCameraImageFormat(QString& format, const bool list = false, QVector<QString>* formatList = nullptr) = 0;

            virtual int setCameraIso(const QString iso) = 0;
            virtual int setCameraExposure(const QString exposure) = 0;
            virtual int setCameraAperture(const QString aperture) = 0;
            virtual int setCameraDriveMode(const QString driveMode) = 0;
            virtual int setCameraFocus(const QString focus) = 0;
            virtual int setCameraWB(const QString wb) = 0;
            virtual int setCameraColorSpace(const QString colorSpace) = 0;
            virtual int setCameraImageFormat(const QString format) = 0;

            virtual void setCaptureParameters(const QString path, const bool bulb = false, const int exposure = 0, const bool onCamera = false) = 0;
            virtual void stopCaptureImage() = 0;

    public slots:
            virtual int captureImage() = 0;

        signals:

            //void imageError(QString err); //error
            void imageAcquired(QString path);
            void captureThreadFinish();
    };
}

#endif // CAMERA_HPP
