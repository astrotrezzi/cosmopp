#ifndef DARKFRAME_H
#define DARKFRAME_H

#include <QWidget>

namespace Ui {
class DarkFrame;
}

class DarkFrame : public QWidget
{
    Q_OBJECT

public:
    explicit DarkFrame(QWidget *parent = nullptr);
    ~DarkFrame();

private:
    Ui::DarkFrame *ui;
};

#endif // DARKFRAME_H
