#ifndef IMAGEHISTO_H
#define IMAGEHISTO_H

#include <QDialog>
#include "include/histo.hpp"

namespace Ui {
class ImageHisto;
}

class ImageHisto : public QDialog
{
    Q_OBJECT

public:
    explicit ImageHisto(QWidget *parent = nullptr);
    ~ImageHisto();

protected slots:
    void on_updateImageHisto(const QString imagePath, const bool enableStatistics);

private:
    Ui::ImageHisto *ui;
    cosmopp::ImageHistogram m_imageHistogram;
    QString m_imagePath = "";
};

#endif // IMAGEHISTO_H
