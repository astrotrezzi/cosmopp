#include "include/cosmoppapp.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CosmoppApp w;
    w.setMaximumHeight(500);
    w.show();
    return a.exec();
}
