#include "../include/imageinfo.h"
#include "ui_imageinfo.h"
#include <QDebug>

static constexpr int ZOOM_DIM_X = 35;
static constexpr int ZOOM_DIM_Y = 35;
static constexpr int ZOOM_X = 6;
static constexpr int ZOOM_Y = 6;

ImageInfo::ImageInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImageInfo)
{
    ui->setupUi(this);
    ui->zoomGraphicsView->setScene(&m_scene);
    m_pixmapItem = m_scene.addPixmap(QPixmap(ZOOM_DIM_X, ZOOM_DIM_Y));
    ui->zoomGraphicsView->scale(ZOOM_X, ZOOM_Y);
    ui->zoomGraphicsView->setMinimumWidth(ZOOM_X * ZOOM_DIM_X + 5);
    ui->zoomGraphicsView->setMinimumHeight(ZOOM_Y * ZOOM_DIM_Y + 5);
    QPen _pen = QPen(Qt::lightGray);
    qreal _penWidth = 0.17;
    _pen.setWidthF(_penWidth);
    m_scene.addRect(ZOOM_DIM_X/2 - _penWidth, ZOOM_DIM_Y/2 - _penWidth, 1 + 2 * _penWidth, 1 + 2 * _penWidth, _pen);
}

ImageInfo::~ImageInfo()
{
    delete ui;
}

int ImageInfo::cropWidth() const
{
    return ZOOM_DIM_X;
}

int ImageInfo::cropHeight() const
{
    return ZOOM_DIM_Y;
}

void ImageInfo::on_updateImageInfo(const QString info)
{
    ui->infoTextEdit->setHtml(info);
}

void ImageInfo::on_updateImageCrop(const QPixmap crop, const QPointF posMouse)
{
    m_pixmapItem->setPixmap(crop);
    QString _txt = QString("Position: %1, %2").arg(posMouse.x()).arg(posMouse.y());
    ui->mousePosLineEdit->setText(_txt);
    QPoint _center(ZOOM_DIM_X/2 + 1, ZOOM_DIM_Y/2 + 1);
    QRgb _mouseColor = crop.toImage().pixel(_center);
    QString _col = QString("Color: R %1, G %2, B %3").arg(qRed(_mouseColor)).arg(qGreen(_mouseColor)).arg(qBlue(_mouseColor));
    ui->mouseColorLineEdit->setText(_col);
}
