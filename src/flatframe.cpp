#include "include/flatframe.h"
#include "ui_flatframe.h"

FlatFrame::FlatFrame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FlatFrame)
{
    ui->setupUi(this);
}

FlatFrame::~FlatFrame()
{
    delete ui;
}
