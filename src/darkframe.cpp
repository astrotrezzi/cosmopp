#include "include/darkframe.h"
#include "ui_darkframe.h"

DarkFrame::DarkFrame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DarkFrame)
{
    ui->setupUi(this);
}

DarkFrame::~DarkFrame()
{
    delete ui;
}
