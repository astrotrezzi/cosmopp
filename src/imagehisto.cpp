#include "include/imagehisto.h"
#include "ui_imagehisto.h"
#include <QDebug>

ImageHisto::ImageHisto(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImageHisto)
{
    ui->setupUi(this);
}

ImageHisto::~ImageHisto()
{
    delete ui;
}

void ImageHisto::on_updateImageHisto(const QString imagePath, const bool enableStatistics)
{
    qDebug() << "Image path:" << imagePath;
    if(imagePath.compare(m_imagePath) != 0)
    {
        m_imagePath = imagePath;
        QtCharts::QChartView* _chartView = nullptr;
        cosmopp::ImageHistogram::ImageStats _imageStats;
        QGraphicsScene _scene;
        if(enableStatistics)
        {
            m_imageHistogram.getImageHisto(m_imagePath, _chartView, _imageStats);
            qDebug() << "peak at (" << _imageStats.maximumBL << "," << _imageStats.maximumPixel << ")";
            qDebug() << "saturated pixels:" << _imageStats.saturatedPixel;
        }
        else
        {
            m_imageHistogram.getImageHisto(m_imagePath, _chartView);
        }
        ui->imageHistoStackedWidget->removeWidget(ui->imageHistoStackedWidget->currentWidget());
        ui->imageHistoStackedWidget->addWidget(_chartView);
    }
}
