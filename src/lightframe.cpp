#include "include/lightframe.h"
#include "ui_lightframe.h"
#include <QDebug>
#include <QDir>
#include <QFileDialog>
#include <QPoint>

LightFrame::LightFrame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LightFrame)
{
    ui->setupUi(this);
}

LightFrame::~LightFrame()
{
    delete ui;
}

void LightFrame::on_newImageAcquired(const AstroImageInfo imageInfo)
{

    if(imageInfo.imageType == COSMOPP_LIGHT_FRAME)
    {
        QString _imageInfo = "";
        _imageInfo += imageInfo.imagePath + "\n";
        _imageInfo += "iso: " + imageInfo.iso + ", ";
        _imageInfo += "aperture: " + imageInfo.aperture + ", ";
        _imageInfo += "exposure (sec): " + imageInfo.exposureTime + "\n";
        _imageInfo += "format: " + imageInfo.format;
        ui->exposureInfoTextEdit->setText(_imageInfo);
        m_imageInfo = imageInfo;
        setImagePath(imageInfo.imagePath);
        if(imageInfo.longExposure)
        {
            int _effectiveIntegrationTime = 0;
            m_frameNum++;
            m_integrationTime = m_integrationTime + m_imageInfo.exposureTime.toInt();
            _effectiveIntegrationTime = m_integrationTime - (ui->wrongFrameSpinBox->value()*m_imageInfo.exposureTime.toInt());
            ui->acquiredFrameLineEdit->setText(QString::number(m_frameNum));
            ui->integrationTimeLineEdit->setText(QString::number(_effectiveIntegrationTime));
        }
        else
        {
            m_frameNum++;
            ui->acquiredFrameLineEdit->setText(QString::number(m_frameNum));
            ui->integrationTimeLineEdit->setText("short exposure");
        }
    }
    else
    {
        qDebug() << "Image skipped! No light frame";
    }
}

void LightFrame::on_updateImageHisto(const QString imagePath, const bool enableStatistics)
{
    qDebug() << "Image path:" << imagePath;
    Q_UNUSED(enableStatistics);
    if(m_imageInfo.imageType == COSMOPP_LIGHT_FRAME)
    {
        setImagePath(imagePath);
        updateHistogram(imagePath);
        updateFrames(imagePath);
        updateStats(true);
    }
}

void LightFrame::on_openFilePushButton_pressed()
{
    QString _homeDir = QDir::homePath();
    QString _filePath = QFileDialog::getOpenFileName(this, tr("Open File"),_homeDir, tr("Images (*.png *.xpm *.jpg)"));
    qDebug() << "current image path:" << _filePath;
    setImagePath(_filePath);
    if(!_filePath.isEmpty())
    {
        ui->exposureInfoTextEdit->clear();
        ui->exposureInfoTextEdit->append("No info available");
        ui->exposureInfoTextEdit->setAlignment(Qt::AlignHCenter);
        updateHistogram(_filePath);
        updateFrames(_filePath);
        updateStats(false);
    }
}

void LightFrame::on_wrongFrameSpinBox_valueChanged(int arg1)
{
    int _effectiveIntegrationTime;
    if(m_imageInfo.longExposure == true)
    {
        _effectiveIntegrationTime = m_integrationTime - (arg1*m_imageInfo.exposureTime.toInt());
        ui->integrationTimeLineEdit->setText(QString::number(_effectiveIntegrationTime));
    }
    else
    {
        ui->integrationTimeLineEdit->setText("short exposure");
    }
}

void LightFrame::on_clearIntPushButton_pressed()
{
    m_integrationTime = 0;
    m_frameNum = 0;
    ui->wrongFrameSpinBox->setValue(0);
    ui->integrationTimeLineEdit->setText("0");
    ui->acquiredFrameLineEdit->setText(QString::number(m_frameNum));
}

void LightFrame::on_currentImgPushButton_pressed()
{
    qDebug() << "current image path:" << m_imageInfo.imagePath;
    setImagePath(m_imageInfo.imagePath);
    updateHistogram(m_imageInfo.imagePath);
    updateFrames(m_imageInfo.imagePath);
    updateStats(true);
}

void LightFrame::updateHistogram(const QString imagePath)
{
    QtCharts::QChartView* _chartView = nullptr;
    cosmopp::ImageHistogram::ImageStats _imageStats;
    QGraphicsScene _scene;
    m_imageHistogram.getImageHisto(imagePath, _chartView, _imageStats);
    qDebug() << "peak at (" << _imageStats.maximumBL << "," << _imageStats.maximumPixel << ")";
    qDebug() << "saturated pixels:" << _imageStats.saturatedPixel;
    m_stats.push_back(_imageStats);
    ui->histoStackedWidget->removeWidget(ui->histoStackedWidget->currentWidget());
    ui->histoStackedWidget->addWidget(_chartView);
}

void LightFrame::updateFrames(const QString imagePath)
{
    qDebug() << "update image frames for" << imagePath;
    QImage _image[5];
    QGraphicsView* _graphicsView[5];
    //original image
    QImage _originalImage;
    _originalImage.load(imagePath);
    int _cropFrameSize = 150;
    //coordinate calculation
    qDebug() << "image width:" << _originalImage.width();
    qDebug() << "image height:" << _originalImage.height();
    qDebug() << "crop size:" << _cropFrameSize;
    _image[static_cast<int>(ImageSub::IMG_CENTRAL)] = getImageSub(_originalImage, _originalImage.width()/2 - _cropFrameSize/2, _originalImage.height()/2 - _cropFrameSize/2,_originalImage.width()/2 + _cropFrameSize/2,_originalImage.height()/2 + _cropFrameSize/2);
    _graphicsView[static_cast<int>(ImageSub::IMG_CENTRAL)] = ui->centralGraphicsView;
    _image[static_cast<int>(ImageSub::IMG_TOP_LEFT)] = getImageSub(_originalImage, 0, 0, _cropFrameSize, _cropFrameSize);
    _graphicsView[static_cast<int>(ImageSub::IMG_TOP_LEFT)] = ui->topLeftGraphicsView;
    _image[static_cast<int>(ImageSub::IMG_TOP_RIGHT)] = getImageSub(_originalImage, _originalImage.width() - _cropFrameSize, 0, _originalImage.width(), _cropFrameSize);
    _graphicsView[static_cast<int>(ImageSub::IMG_TOP_RIGHT)] = ui->topRightGraphicsView;
    _image[static_cast<int>(ImageSub::IMG_BOTTOM_LEFT)] = getImageSub(_originalImage, 0, _originalImage.height() - _cropFrameSize, _cropFrameSize, _originalImage.height());
    _graphicsView[static_cast<int>(ImageSub::IMG_BOTTOM_LEFT)] = ui->bottomLeftGraphicsView;
    _image[static_cast<int>(ImageSub::IMG_BOTTOM_RIGHT)] = getImageSub(_originalImage, _originalImage.width() - _cropFrameSize, _originalImage.height() - _cropFrameSize, _originalImage.width(), _originalImage.height());
    _graphicsView[static_cast<int>(ImageSub::IMG_BOTTOM_RIGHT)] = ui->bottomRightGraphicsView;
    for(int i = 0; i < NSUBFRAMES; i++)
    {
        m_subFrameScene[i].clear();
        _graphicsView[i]->setScene(&m_subFrameScene[i]);
        m_subFrame[i] = m_subFrameScene[i].addPixmap(QPixmap::fromImage(_image[i]));
        _graphicsView[i]->fitInView(m_subFrame[i],Qt::KeepAspectRatio);
    }
}

QImage LightFrame::getImageSub(const QImage original, const int topleftX, const int topleftY, const int bottomRightX, const int bottomRightY)
{
    int _subFrameCoord[4];
    _subFrameCoord[0] = topleftX;
    _subFrameCoord[1] = topleftY;
    _subFrameCoord[2] = bottomRightX;
    _subFrameCoord[3] = bottomRightY;
    QRect _subFrameRect(QPoint(_subFrameCoord[0], _subFrameCoord[1]), QPoint(_subFrameCoord[2], _subFrameCoord[3]));
    QImage _subFrame = original.copy(_subFrameRect);
    return _subFrame;
}

void LightFrame::updateStats(const bool enable)
{
    ImageQuality _quality = ImageQuality::IMGQ_NOT_AVAILABLE;
    ImageTrend _trend = ImageTrend::IMGT_NOT_AVAILABLE;
    if(enable)
    {
        getFocusQuality(_quality);
        setQualityStatus(ui->focusQualityLineEdit, _quality);
        getSkyConditions(_quality);
        setQualityStatus(ui->skyConditionsLineEdit, _quality);
        getTrackingQuality(_quality);
        setQualityStatus(ui->trackingQualityLineEdit, _quality);
        getGradients(_quality);
        setQualityStatus(ui->gradientsLineEdit, _quality);
        _trend = getExposureTrend();
        setTrendStatus(ui->exposureTrendLabel, _trend);
        _trend = getHotPixels();
        setTrendStatus(ui->hotPixelsLabel, _trend);
    }
    else
    {
        setQualityStatus(ui->focusQualityLineEdit, _quality);
        setQualityStatus(ui->skyConditionsLineEdit, _quality);
        setQualityStatus(ui->trackingQualityLineEdit, _quality);
        setQualityStatus(ui->gradientsLineEdit, _quality);
        setTrendStatus(ui->exposureTrendLabel, _trend);
        setTrendStatus(ui->hotPixelsLabel, _trend);
    }
}

void LightFrame::getFocusQuality(LightFrame::ImageQuality &quality)
{
    quality = ImageQuality::IMGQ_NOT_AVAILABLE;
}

void LightFrame::getSkyConditions(LightFrame::ImageQuality &quality)
{
    quality = ImageQuality::IMGQ_NOT_AVAILABLE;
}

void LightFrame::getTrackingQuality(LightFrame::ImageQuality &quality)
{
    quality = ImageQuality::IMGQ_NOT_AVAILABLE;
}

void LightFrame::getGradients(LightFrame::ImageQuality &quality)
{
    quality = ImageQuality::IMGQ_NOT_AVAILABLE;
}

LightFrame::ImageTrend LightFrame::getExposureTrend()
{
    ImageTrend _imageTrend = ImageTrend::IMGT_NOT_AVAILABLE;
    size_t _Nframes = 0;
    _Nframes = m_stats.size();
    if(_Nframes > 1)
    {
        if(m_stats.at(_Nframes - 1).maximumBL > m_stats.at(_Nframes - 2).maximumBL)
        {
            qDebug() << "Exposure increasing";
            _imageTrend = ImageTrend::IMGT_INCREASE;
        }
        else if (m_stats.at(_Nframes - 1).maximumBL < m_stats.at(_Nframes - 2).maximumBL)
        {
            qDebug() << "Exposure decreasing";
            _imageTrend = ImageTrend::IMGT_DECREASE;
        }
        else
        {
            qDebug() << "Exposure stable";
            _imageTrend = ImageTrend::IMGT_STABLE;
        }
    }
    return _imageTrend;
}

LightFrame::ImageTrend LightFrame::getHotPixels()
{
    ImageTrend _imageTrend = ImageTrend::IMGT_NOT_AVAILABLE;
    size_t _Nframes = 0;
    _Nframes = m_stats.size();
    qDebug() << "Number of frames:" << _Nframes;
    if(_Nframes > 1)
    {
        if(m_stats.at(_Nframes - 1).saturatedPixel > m_stats.at(_Nframes - 2).saturatedPixel)
        {
            _imageTrend = ImageTrend::IMGT_INCREASE;
        }
        else if (m_stats.at(_Nframes - 1).saturatedPixel < m_stats.at(_Nframes - 2).saturatedPixel)
        {
            _imageTrend = ImageTrend::IMGT_DECREASE;
        }
        else
        {
            _imageTrend = ImageTrend::IMGT_STABLE;
        }
    }
    return _imageTrend;
}

void LightFrame::setQualityStatus(QLineEdit*& lineEdit, const LightFrame::ImageQuality quality)
{
    QString _styleSheet = "";
    switch (quality)
    {
    case ImageQuality::IMGQ_EXCELLENT:
        _styleSheet = "background-color: rgb(0, 104, 55);";
        break;

    case ImageQuality::IMGQ_GOOD:
        _styleSheet = "background-color: rgb(58, 181, 75);";
        break;

    case ImageQuality::IMGQ_FAIR:
        _styleSheet = "background-color: rgb(216, 223, 32);";
        break;

    case ImageQuality::IMGQ_POOR:
        _styleSheet = "background-color: rgb(250, 175, 58);";
        break;

    case ImageQuality::IMGQ_VERY_POOR:
        _styleSheet = "background-color: rgb(241, 90, 37);";
        break;

    case ImageQuality::IMGQ_NOT_AVAILABLE:
        _styleSheet = "background-color: rgb(160, 160, 160);";
        break;
    }
    lineEdit->setStyleSheet(_styleSheet);
}

void LightFrame::setTrendStatus(QLabel*& label, const LightFrame::ImageTrend trend)
{
    QPixmap _trendIcon;
    switch(trend)
    {
    case ImageTrend::IMGT_INCREASE:
        _trendIcon.load(":/images/trend/up.png");
        break;

    case ImageTrend::IMGT_DECREASE:
        _trendIcon.load(":/images/trend/down.png");
        break;

    case ImageTrend::IMGT_STABLE:
        _trendIcon.load(":/images/trend/stable.png");
        break;

    case ImageTrend::IMGT_NOT_AVAILABLE:
        _trendIcon.load(":/images/trend/unknown.png");
        break;
    }
    label->setPixmap(_trendIcon);
}

void LightFrame::on_clearStatsToolButton_pressed()
{
    m_stats.clear();
    this->updateStats(false);
}
