#include "include/histo.hpp"
#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>

static constexpr int HISTO_SIZE = 256;

cosmopp::ImageHistogram::ImageHistogram(QWidget *parent)
    : QObject(parent)
{
}

cosmopp::ImageHistogram::~ImageHistogram()
{
}

void cosmopp::ImageHistogram::getImageHisto(const QString imagePath, QtCharts::QChartView *&plot, ImageHistogram::ImageStats& imageStat)
{
    int _histo[HISTO_SIZE];
    createImage(imagePath);
    imageProcess(_histo);
    createHisto(_histo, plot);
    imageStat = imageStats(_histo);
}

void cosmopp::ImageHistogram::getImageHisto(const QString imagePath, QtCharts::QChartView *&plot)
{
    int _histo[HISTO_SIZE];
    createImage(imagePath);
    imageProcess(_histo);
    createHisto(_histo, plot);
}

void cosmopp::ImageHistogram::createImage(const QString imagePath)
{
    QImage _image(imagePath);
    m_imagePix = _image;
    m_image = m_scene.addPixmap(QPixmap::fromImage(m_imagePix));
}

void cosmopp::ImageHistogram::imageProcess(int* histo)
{
    int _width = 0;
    int _height = 0;
    QRgb _color;
    int _red = 0;
    int _green = 0;
    int _blue = 0;
    int _luminance = 0;
    QPixmap _image = QPixmap::fromImage(m_imagePix);
    for(int i = 0; i < HISTO_SIZE; i++)
        histo[i] = 0;
    _width = m_imagePix.width();
    _height = m_imagePix.height();
    for(int i = 0; i < _width; i++)
    {
        for(int j = 0; j < _height; j++)
        {
            _color = _image.toImage().pixel(i,j);
            _green = qGreen(_color);
            _red = qRed(_color);
            _blue = qBlue(_color);
            _luminance = (_red + _blue + _green) / 3;
            if(_luminance < HISTO_SIZE)
                histo[_luminance]++;
        }
    }
}

void cosmopp::ImageHistogram::createHisto(const int* histo, QtCharts::QChartView*& plot)
{
    QtCharts::QLineSeries *series = new QtCharts::QLineSeries();
    for(int i = 0; i < HISTO_SIZE; i++)
        series->append(i,histo[i]);

    QtCharts::QChart *chart = new QtCharts::QChart();
    chart->addSeries(series);
    chart->setAnimationOptions(QtCharts::QChart::AllAnimations);

    chart->legend()->setVisible(false);

    QtCharts::QValueAxis *axisX = new QtCharts::QValueAxis();
    axisX->setTitleText("Brightness Levels");
    axisX->setLabelFormat("%i");
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);

    QtCharts::QValueAxis *axisY = new QtCharts::QValueAxis();
    axisY->setTitleText("Pixels");
    axisY->setLabelFormat("%i");
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);

    QtCharts::QChartView *chartView = new QtCharts::QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);

    plot = chartView;
}

cosmopp::ImageHistogram::ImageStats cosmopp::ImageHistogram::imageStats(const int *histo)
{
    int _maximum = 0;
    int _maximumIdx = 0;
    int _saturated = 0;
    ImageStats _imageStats;
    for(int i = 0; i < HISTO_SIZE; i++)
    {
        if(histo[i] > _maximum)
        {
            _maximumIdx = i;
            _maximum = histo[i];
        }
    }
    _saturated = histo[HISTO_SIZE-1];
    _imageStats.maximumBL = _maximumIdx;
    _imageStats.maximumPixel = _maximum;
    _imageStats.saturatedPixel = _saturated;
    return _imageStats;
}
