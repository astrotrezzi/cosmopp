#include "include/gcamera.hpp"
#include "include/gcameraerror.hpp"
#include <QThread>
#include <QDebug>

cosmopp::GCamera::GCamera()
{
    initCamera();
}

cosmopp::GCamera::~GCamera()
{

}

void cosmopp::GCamera::initCamera()
{
    //unmount camera from Linux OS
    qDebug() << m_camerainitCmd;
    m_gphotoProcess.start(m_camerainitCmd);
    m_gphotoProcess.waitForFinished();
}

int cosmopp::GCamera::getCameraModel(QString &cameraName)
{
    int _ret = GCAMERA_NO_ERROR;
    QString _cameraName;
    _ret = this->getConfig("/main/status/cameramodel",_cameraName);
    cameraName = _cameraName;
    return _ret;
}

int cosmopp::GCamera::getBatteryStatus(int &batteryLevel)
{
    int _ret = GCAMERA_NO_ERROR;
    QString _batteryLevelString;
    _ret = this->getConfig("/main/status/batterylevel",_batteryLevelString);
    std::string _batteryLevelStdString = _batteryLevelString.toStdString();
    _batteryLevelStdString.pop_back();
    _batteryLevelString = QString::fromStdString(_batteryLevelStdString);
    batteryLevel = _batteryLevelString.toInt();
    return _ret;
}

int cosmopp::GCamera::getCameraIso(QString &iso, const bool list, QVector<QString>* isoList)
{
    int _ret = GCAMERA_NO_ERROR;
    QString _cmd = "/main/imgsettings/iso";
    QVector<QString> _isoList;
    if(list)
    {
        _ret = this->getConfig(_cmd, iso, true, &_isoList);
        *isoList = _isoList;
    }
    else
    {
        _ret = this->getConfig(_cmd, iso);
    }
    return _ret;
}

int cosmopp::GCamera::getCameraExposure(QString& exposure, const bool list, QVector<QString>* exposureList)
{
    int _ret = GCAMERA_NO_ERROR;
    QString _cmd = "/main/capturesettings/shutterspeed";
    QVector<QString> _exposureList;
    QVector<QString> _exposureListOffset;
    std::string _exposureString = "";
    if(list)
    {
        _ret = this->getConfig(_cmd, exposure, true, &_exposureList);
        for(int i = 0; i < _exposureList.size(); i++)
        {
            if(i > 9)
            {
                //remove empy spaces
                _exposureString = _exposureList.at(i).toStdString();
                _exposureString.erase(0,1);
                _exposureListOffset.push_back(QString::fromStdString(_exposureString));
            }
            else
            {
                _exposureListOffset.push_back(_exposureList.at(i));
            }
        }
        *exposureList = _exposureListOffset;
    }
    else
    {
        _ret = this->getConfig(_cmd, exposure);
    }
    return _ret;
}

int cosmopp::GCamera::getCameraAperture(QString &aperture, const bool list, QVector<QString>* apertureList)
{
    int _ret = GCAMERA_NO_ERROR;
    QString _cmd = "/main/capturesettings/aperture";
    QVector<QString> _apertureList;
    QVector<QString> _apertureListFloat;
    if(list)
    {
        _ret = this->getConfig(_cmd, aperture, true, &_apertureList);
        for(int i = 0; i < _apertureList.size(); i++)
        {
            double _aperture = 0;
            _aperture = _apertureList.at(i).toDouble();
            _apertureListFloat.push_back(QString::number(_aperture));
        }
        *apertureList = _apertureListFloat;
    }
    else
    {
        _ret = this->getConfig(_cmd, aperture);
    }
    return _ret;
}

int cosmopp::GCamera::getCameraDriveMode(QString &driveMode, const bool list, QVector<QString>* driveModeList)
{
    int _ret = GCAMERA_NO_ERROR;
    QString _cmd = "/main/capturesettings/drivemode";
    QVector<QString> _driveModeList;
    if(list)
    {
        _ret = this->getConfig(_cmd, driveMode, true, &_driveModeList);
        *driveModeList = _driveModeList;
    }
    else
    {
        _ret = this->getConfig(_cmd, driveMode);
    }
    return _ret;
}

int cosmopp::GCamera::getCameraFocus(QString &focus, const bool list, QVector<QString>* focusList)
{
    int _ret = GCAMERA_NO_ERROR;
    QString _cmd = "/main/capturesettings/focusmode";
    QVector<QString> _focusList;
    if(list)
    {
        _ret = this->getConfig(_cmd, focus, true, &_focusList);
        *focusList = _focusList;
    }
    else
    {
        _ret = this->getConfig(_cmd, focus);
    }
    return _ret;
}

int cosmopp::GCamera::getCameraWB(QString &wb, const bool list, QVector<QString>* wbList)
{
    int _ret = GCAMERA_NO_ERROR;
    QString _cmd = "/main/imgsettings/whitebalance";
    QVector<QString> _wbList;
    if(list)
    {
        _ret = this->getConfig(_cmd, wb, true, &_wbList);
        *wbList = _wbList;
    }
    else
    {
        _ret = this->getConfig(_cmd, wb);
    }
    return _ret;
}

int cosmopp::GCamera::getCameraColorSpace(QString &colorSpace, const bool list, QVector<QString>* colorSpaceList)
{
    int _ret = GCAMERA_NO_ERROR;
    QString _cmd = "/main/imgsettings/colorspace";
    QVector<QString> _colorSpaceList;
    if(list)
    {
        _ret = this->getConfig(_cmd, colorSpace, true, &_colorSpaceList);
        *colorSpaceList = _colorSpaceList;
    }
    else
    {
        _ret = this->getConfig(_cmd, colorSpace);
    }
    return _ret;
}

int cosmopp::GCamera::getCameraImageFormat(QString &format, const bool list, QVector<QString>* formatList)
{
    int _ret = GCAMERA_NO_ERROR;
    QString _cmd = "/main/imgsettings/imageformat";
    QVector<QString> _formatList;
    if(list)
    {
        _ret = this->getConfig(_cmd, format, true, &_formatList);
        *formatList = _formatList;
    }
    else
    {
        _ret = this->getConfig(_cmd, format);
    }
    return _ret;
}

int cosmopp::GCamera::setCameraIso(const QString iso)
{
    int _ret = GCAMERA_NO_ERROR;
    _ret = this->setConfig("/main/imgsettings/iso",iso);
    return _ret;
}

int cosmopp::GCamera::setCameraExposure(const QString exposure)
{
    int _ret = GCAMERA_NO_ERROR;
    _ret = this->setConfig("/main/capturesettings/shutterspeed",exposure);
    return _ret;
}

int cosmopp::GCamera::setCameraAperture(const QString aperture)
{
    int _ret = GCAMERA_NO_ERROR;
    _ret = this->setConfig("/main/capturesettings/aperture",aperture);
    return _ret;
}

int cosmopp::GCamera::setCameraDriveMode(const QString driveMode)
{
    int _ret = GCAMERA_NO_ERROR;
    _ret = this->setConfig("/main/capturesettings/drivemode",driveMode);
    return _ret;
}

int cosmopp::GCamera::setCameraFocus(const QString focus)
{
    int _ret = GCAMERA_NO_ERROR;
    _ret = this->setConfig("/main/capturesettings/focusmode",focus);
    return _ret;
}

int cosmopp::GCamera::setCameraWB(const QString wb)
{
    int _ret = GCAMERA_NO_ERROR;
    _ret = this->setConfig("/main/imgsettings/whitebalance",wb);
    return _ret;
}

int cosmopp::GCamera::setCameraColorSpace(const QString colorSpace)
{
    int _ret = GCAMERA_NO_ERROR;
    _ret = this->setConfig("/main/imgsettings/colorspace",colorSpace);
    return _ret;
}

int cosmopp::GCamera::setCameraImageFormat(const QString format)
{
    int _ret = GCAMERA_NO_ERROR;
    _ret = this->setConfig("/main/imgsettings/imageformat",format);
    return _ret;
}

void cosmopp::GCamera::setCaptureParameters(const QString path, const bool bulb, const int exposure, const bool onCamera)
{
    m_path = path;
    m_bulb = bulb;
    m_exposure = exposure;
    m_onCamera = onCamera;
}

int cosmopp::GCamera::captureImage()
{
    QString _cmd = "";
    QProcess _gphotoProcess;
    m_exposureInter = false;
    if(m_bulb)
    {
        if(m_onCamera)
            _cmd = m_gphoto2Cmd + m_forceOverwriteCmd + " " + m_keepCmd + " " + m_filenameCmd + m_path + " " + m_setConfigCmd + " eosremoterelease=Immediate --wait-event=" + QString::number(m_exposure) + "s " + m_setConfigCmd + " eosremoterelease=\"Release Full\"" + " --wait-event-and-download=2s";
        else
            _cmd = m_gphoto2Cmd + m_forceOverwriteCmd + " " + m_filenameCmd + m_path + " " + m_setConfigCmd + " eosremoterelease=Immediate --wait-event=" + QString::number(m_exposure) + "s " + m_setConfigCmd + " eosremoterelease=\"Release Full\"" + " --wait-event-and-download=2s";
    }
    else
    {
        if(m_onCamera)
            _cmd = m_gphoto2Cmd + m_forceOverwriteCmd + " " + m_keepCmd + " " + m_filenameCmd + m_path + " " + m_captureImgCmd;
        else
            _cmd = m_gphoto2Cmd + m_forceOverwriteCmd + " " + m_filenameCmd + m_path + " " + m_captureImgCmd;
    }
    qDebug() << _cmd;
    initCamera();
    _gphotoProcess.start(_cmd);
    if(m_bulb)
    {
        for(int i = 0; i < m_exposure; i++)
        {
            QThread::sleep(1);
            if(m_exposureInter)
            {
                _gphotoProcess.terminate();
                break;
            }
        }
        QThread::sleep(DOWNLOAD_DELAY_TIME);
    }
    else
    {
        _gphotoProcess.waitForFinished();
    }
    QString _outputCmd(_gphotoProcess.readAllStandardOutput());
    std::string _path = "";
    _path = m_path.toStdString();
    _path.pop_back();
    _path.pop_back();
    _path.pop_back();
    emit imageAcquired(QString::fromStdString(_path));
    emit captureThreadFinish();
    return GCAMERA_NO_ERROR;
}

void cosmopp::GCamera::stopCaptureImage()
{
    m_exposureInter = true;
}

std::vector<size_t> cosmopp::GCamera::findAll(const QString str, const QString sub)
{
    std::string _str = "", _sub = "";
    _str = str.toStdString();
    _sub = sub.toStdString();
    std::vector<size_t> _positions = {};
    size_t _pos = _str.find(_sub, 0);
    while(_pos != std::string::npos)
    {
        _positions.push_back(_pos);
        _pos = _str.find(_sub,_pos+1);
    }
    return _positions;
}

int cosmopp::GCamera::getConfig(const QString cmd, QString& output, const bool moreOptions, QVector<QString>* options)
{
    int _ret = GCAMERA_NO_ERROR;
    qDebug() << m_gphoto2Cmd + m_getConfigCmd + cmd;
    initCamera();
    m_gphotoProcess.start(m_gphoto2Cmd + m_getConfigCmd + cmd);
    m_gphotoProcess.waitForFinished();
    QString _outputCmd(m_gphotoProcess.readAllStandardOutput());
    size_t _first = 0, _last = 0, _size = 0;
    std::vector<size_t> _firstV = {}, _lastV = {};
    //find cmd output
    _firstV = this->findAll(_outputCmd,"Current: ");
    if(!_firstV.empty())
    {
        _first = _firstV.at(0);
        _lastV = this->findAll(_outputCmd,"\n");
        if(!_lastV.empty())
        {
            _last  = _lastV.at(3);
            _size = _last - _first;
            output = _outputCmd.mid(_first+9,_size-9);
            //find more options
            if(moreOptions)
            {
                QString _option = "";
                std::vector<size_t> _tmpVect;
                int _optionNum = 0;
                _tmpVect = this->findAll(_outputCmd,"\n");
                if(!_tmpVect.empty())
                {
                    _optionNum = _tmpVect.size();
                    _optionNum = _optionNum - 5;
                    for(int i = 0; i < _optionNum; i++)
                    {
                        _first = this->findAll(_outputCmd,"Choice: ").at(i);
                        _last  = this->findAll(_outputCmd,"\n").at(i + 4);
                        _size = _last - _first;
                        _option = _outputCmd.mid(_first+10,_size-10);
                        options->push_back(_option);
                    }
                }
                else
                {
                    _ret = GCAMERA_PROTOCOL_ERROR;
                }
            }
        }
        else
        {
            _ret = GCAMERA_PROTOCOL_ERROR;
        }
    }
    else
    {
        _ret = GCAMERA_COMMUNICATION_ERROR;
    }
    return _ret;
}

int cosmopp::GCamera::setConfig(const QString cmd, const QString &input)
{
    qDebug() << m_gphoto2Cmd + m_setConfigCmd + " " + cmd + "=" + input;
    initCamera();
    m_gphotoProcess.start(m_gphoto2Cmd + m_setConfigCmd + " " + cmd + "=" + input);
    m_gphotoProcess.waitForFinished();
    QString _outputCmd(m_gphotoProcess.readAllStandardOutput());
    return GCAMERA_NO_ERROR;
}
