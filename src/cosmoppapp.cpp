#include "include/cosmoppapp.h"
#include "ui_cosmoppapp.h"
#include <QTimer>
#include <QFileDialog>
#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QDateTime>
#include <QGraphicsSceneMouseEvent>
#include <QElapsedTimer>

CosmoppApp::CosmoppApp(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::CosmoppApp)
{
    m_camera = new cosmopp::GCamera();
    ui->setupUi(this);
    m_battery_level = new QLabel();
    m_battery_percentage = new QLabel();
    m_batteryWidget = new QWidget();
    m_statusBarLayout = new QHBoxLayout(m_batteryWidget);
    m_cameraModel = new QLabel();
    m_spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
    checkBattery();
    initStatusBar();
    initIso();
    initExposure();
    initAperture();
    initDriveMode();
    initFocus();
    initWb();
    initColorSpace();
    initImageFormat();
    connect(m_camera, &cosmopp::GCamera::imageAcquired, this, &CosmoppApp::on_showImageAcquired );
    m_batteryTimer = new QTimer(this);
    connect(m_batteryTimer, SIGNAL(timeout()), this, SLOT(updateBattery()));
    m_batteryTimer->start(BATTERY_LEVEL_TIMEOUT);
    m_captureThread = new QThread();
    m_camera->moveToThread(m_captureThread);
    connect(m_captureThread, SIGNAL (started()), m_camera, SLOT (captureImage()));
    connect(m_camera, SIGNAL (captureThreadFinish()), m_captureThread, SLOT (quit()));
    connect(m_camera, SIGNAL (captureThreadFinish()), this, SLOT(on_seqCaptureThreadFinished()));
    m_imageDir = QDir::homePath() + "/";
    m_imageFileName = "img";
    m_imageFileFormat =".%C";
    m_imageIndex = 0;
    m_imagePath = m_imageDir + m_imageFileName + "_" + QString::number(m_imageIndex) + m_imageFileFormat;
    ui->frameNumLineEdit->setText(QString::number(m_imageIndex));
    QImage _image(":/images/init_preview.jpg");
    ui->imagePreview->setScene(&m_preview_scene);
    ui->imageSequence->setScene(&m_sequence_scene);
    m_preview = m_preview_scene.addPixmap(QPixmap::fromImage(_image));
    m_sequence = m_sequence_scene.addPixmap(QPixmap::fromImage(_image));
    ui->cameraTabWidget->setCurrentIndex(0);
    connect(this, SIGNAL(exeSequence(int)),this, SLOT(on_exeSequence(int)));
    //image Info Dialog
    m_imageInfo = new ImageInfo(this);
    m_imageInfo->setWindowTitle("Zoom");
    m_imageInfo->setWindowIcon(QIcon(":/images/zoom.png"));
    connect(this, SIGNAL(updateImageInfo(QString)), m_imageInfo, SLOT(on_updateImageInfo(QString)));
    connect(this, SIGNAL(updateImageCrop(const QPixmap, const QPointF)), m_imageInfo, SLOT(on_updateImageCrop(const QPixmap, const QPointF)));
    m_preview_scene.installEventFilter(this);
    ui->imagePreview->setMouseTracking(true);
    m_sequence_scene.installEventFilter(this);
    ui->imageSequence->setMouseTracking(true);
    //image Histogram Dialog
    m_imageHisto = new ImageHisto(this);
    m_imageHisto->setWindowTitle("Image Histogram");
    m_imageHisto->setWindowIcon(QIcon(":/images/histogram.png"));
    connect(this, SIGNAL(updateImageHistoTool(QString, bool)), m_imageHisto, SLOT(on_updateImageHisto(QString, bool)));
    //Light Frame Object
    m_lightFrame = new LightFrame();
//    ui->cameraTabWidget->addTab(m_lightFrame, "Light");
    connect(this, SIGNAL(newImageAcquired(AstroImageInfo)), m_lightFrame, SLOT(on_newImageAcquired(AstroImageInfo)));
    connect(this, SIGNAL(updateImageHisto(QString, bool)), m_lightFrame, SLOT(on_updateImageHisto(QString, bool)));
    //Flat Frame Object
    m_flatFrame = new FlatFrame();
//    ui->cameraTabWidget->addTab(m_flatFrame, "Flat");
    //Bias Frame Object
    m_biasFrame = new BiasFrame();
//    ui->cameraTabWidget->addTab(m_biasFrame, "Bias");
    //Dark Frame Object
    m_darkFrame = new DarkFrame();
//    ui->cameraTabWidget->addTab(m_darkFrame, "Dark");
}

CosmoppApp::~CosmoppApp()
{
    m_batteryTimer->stop();
    disconnect(m_camera, &cosmopp::GCamera::imageAcquired, this, &CosmoppApp::on_showImageAcquired );
    delete m_darkFrame;
    delete m_biasFrame;
    delete m_flatFrame;
    delete m_lightFrame;
    delete m_imageHisto;
    delete m_imageInfo;
    delete m_batteryTimer;
    delete m_camera;
    delete m_statusBarLayout;
    delete ui;
}

void CosmoppApp::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    ui->imagePreview->fitInView(m_preview, Qt::KeepAspectRatio);
    ui->imageSequence->fitInView(m_sequence, Qt::KeepAspectRatio);
}

void CosmoppApp::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
    ui->imagePreview->fitInView(m_preview, Qt::KeepAspectRatio);
    ui->imageSequence->fitInView(m_sequence, Qt::KeepAspectRatio);
}

bool CosmoppApp::eventFilter(QObject *obj, QEvent *ev)
{
    Q_UNUSED(obj);
    static QElapsedTimer ET;
    static QPointF _lastPos;
    QPointF _topLeft;
    int _dimX = 0;
    int _dimY = 0;
    QPixmap _crop;
    if(ev->type() == QEvent::GraphicsSceneMouseMove)
    {
        QGraphicsSceneMouseEvent* _moveMouse = static_cast<QGraphicsSceneMouseEvent*>(ev);
        QLineF _delta(_moveMouse->scenePos(), _lastPos);
        ET.restart();
        _lastPos = _moveMouse->scenePos();
        if(m_isSequenceTab)
        {
            if(!m_sequence->contains(_moveMouse->scenePos()))
                return true;
            _dimX = m_imageInfo->cropWidth();
            _dimY = m_imageInfo->cropHeight();
            _topLeft = _moveMouse->scenePos();
            _topLeft -= QPointF(_dimX/2, _dimY/2);
            QRect rect(_topLeft.toPoint(), QSize(_dimX, _dimY));
            _crop = m_sequence->pixmap().copy(rect);
            emit updateImageCrop(_crop, _moveMouse->scenePos());
        }
        else
        {
            if(!m_preview->contains(_moveMouse->scenePos()))
                return true;
            _dimX = m_imageInfo->cropWidth();
            _dimY = m_imageInfo->cropHeight();
            _topLeft = _moveMouse->scenePos();
            _topLeft -= QPointF(_dimX/2, _dimY/2);
            QRect rect(_topLeft.toPoint(), QSize(_dimX, _dimY));
            _crop = m_preview->pixmap().copy(rect);
            emit updateImageCrop(_crop, _moveMouse->scenePos());
        }
        return true;
    }
    return false;
}

void CosmoppApp::on_setIsoPushButton_pressed()
{
    m_camera->setCameraIso(QString::number(ui->setIsoComboBox->currentIndex()));
    QString _iso = "";
    m_camera->getCameraIso(_iso);
    ui->isoLineEdit->setText(_iso);
}

void CosmoppApp::on_setExposurePushButton_pressed()
{
    int _idx = ui->setExposureComboBox->findText(QString::number(ui->setExposureComboBox->currentIndex()));
    qDebug() << "current index:'" << ui->setExposureComboBox->currentIndex() << "'";
    qDebug() << "current value:'" << ui->setExposureComboBox->currentText() << "'";
    qDebug() << "found index:" << _idx;
    if(_idx != -1)
        m_camera->setCameraExposure(ui->setExposureComboBox->currentText());
    else
        m_camera->setCameraExposure(QString::number(ui->setExposureComboBox->currentIndex()));
    QString _exposure = "";
    m_camera->getCameraExposure(_exposure);
    ui->exposureLineEdit->setText(_exposure);
    bool _longExposureEnabled = false;
    _longExposureEnabled = _exposure.compare("bulb") == 0 ? true : false;
    ui->minSpinBox->setEnabled(_longExposureEnabled);
    ui->secSpinBox->setEnabled(_longExposureEnabled);
}

void CosmoppApp::on_setAperturePushButton_pressed()
{
    int _idx = ui->setApertureComboBox->findText(QString::number(ui->setApertureComboBox->currentIndex()));
    if(_idx != -1)
        m_camera->setCameraAperture(ui->setApertureComboBox->currentText());
    else
        m_camera->setCameraAperture(QString::number(ui->setApertureComboBox->currentIndex()));
    QString _aperture = "";
    m_camera->getCameraAperture(_aperture);
    ui->apertureLineEdit->setText(_aperture);
}

void CosmoppApp::on_setDrivemodePushButton_pressed()
{
    m_camera->setCameraDriveMode(QString::number(ui->setDrivemodeComboBox->currentIndex()));
    QString _driveMode = "";
    m_camera->getCameraDriveMode(_driveMode);
    ui->drivemodeLineEdit->setText(_driveMode);
}

void CosmoppApp::on_setFocusPushButton_pressed()
{
    m_camera->setCameraFocus(QString::number(ui->setFocusComboBox->currentIndex()));
    QString _focus = "";
    m_camera->getCameraFocus(_focus);
    ui->focusLineEdit->setText(_focus);
}

void CosmoppApp::on_setWbPushButton_pressed()
{
    m_camera->setCameraWB(QString::number(ui->setWbComboBox->currentIndex()));
    QString _wb = "";
    m_camera->getCameraWB(_wb);
    ui->wbLineEdit->setText(_wb);
}

void CosmoppApp::on_setColorspacePushButton_pressed()
{
    m_camera->setCameraColorSpace(QString::number(ui->setColorspaceComboBox->currentIndex()));
    QString _colorSpace = "";
    m_camera->getCameraColorSpace(_colorSpace);
    ui->colorspaceLineEdit->setText(_colorSpace);
}

void CosmoppApp::on_setImgformatPushButton_pressed()
{
    m_camera->setCameraImageFormat(QString::number(ui->setImgformatComboBox->currentIndex()));
    QString _imageFormat = "";
    m_camera->getCameraImageFormat(_imageFormat);
    ui->imgformatLineEdit->setText(_imageFormat);
}

void CosmoppApp::initStatusBar()
{
    QString _model = "";
    m_camera->getCameraModel(_model);
    m_cameraModel->setText(_model);
    m_statusBarLayout->setContentsMargins(3,0,3,0);
    m_statusBarLayout->setSpacing(6);
    m_statusBarLayout->addWidget(m_cameraModel);
    m_statusBarLayout->addItem(m_spacer);
    m_statusBarLayout->addWidget(m_battery_percentage);
    m_statusBarLayout->addWidget(m_battery_level);
    ui->statusbar->addWidget(m_batteryWidget,1);
    ui->statusbar->setMaximumHeight(30);
}

void CosmoppApp::checkBattery()
{
    int _batteryLevel = 0;
    QPixmap _batteryIcon;
    m_camera->getBatteryStatus(_batteryLevel);
    m_battery_percentage->setText(QString::number(_batteryLevel) + "%");
    if(_batteryLevel > 80)
        _batteryIcon = QPixmap(":/images/battery-status/100.png");
    else if((_batteryLevel <= 80) && (_batteryLevel > 60))
        _batteryIcon = QPixmap(":/images/battery-status/80.png");
    else if((_batteryLevel <= 60) && (_batteryLevel > 40))
        _batteryIcon = QPixmap(":/images/battery-status/60.png");
    else if((_batteryLevel <= 40) && (_batteryLevel > 20))
        _batteryIcon = QPixmap(":/images/battery-status/40.png");
    else if(_batteryLevel <= 20)
        _batteryIcon = QPixmap(":/images/battery-status/20.png");
    m_battery_level->setPixmap(_batteryIcon.scaled(30,30,Qt::KeepAspectRatio));
}

void CosmoppApp::initIso()
{
    QString _iso = "";
    QVector<QString> _isoList;
    m_camera->getCameraIso(_iso, true, &_isoList);
    ui->isoLineEdit->setText(_iso);
    for(int i = 0; i < _isoList.size(); i++)
        ui->setIsoComboBox->addItem(_isoList.at(i),i);
    ui->setIsoComboBox->setCurrentText(_iso);
}

void CosmoppApp::initExposure()
{
    QString _exposure = "";
    QVector<QString> _exposureList;
    m_camera->getCameraExposure(_exposure, true, &_exposureList);
    ui->exposureLineEdit->setText(_exposure);
    for(int i = 0; i < _exposureList.size(); i++)
        ui->setExposureComboBox->addItem(_exposureList.at(i),i);
    ui->setExposureComboBox->setCurrentText(_exposure);
    bool _longExposureEnabled = false;
    _longExposureEnabled = _exposure.compare("bulb") == 0 ? true : false;
    ui->minSpinBox->setEnabled(_longExposureEnabled);
    ui->secSpinBox->setEnabled(_longExposureEnabled);
}

void CosmoppApp::initAperture()
{
    QString _aperture = "";
    QVector<QString> _apertureList;
    m_camera->getCameraAperture(_aperture, true, &_apertureList);
    ui->apertureLineEdit->setText(_aperture);
    for(int i = 0; i < _apertureList.size(); i++)
    {
        ui->setApertureComboBox->addItem(_apertureList.at(i),i);
    }
    ui->setApertureComboBox->setCurrentText(_aperture);
}

void CosmoppApp::initDriveMode()
{
    QString _driveMode = "";
    QVector<QString> _driveModeList;
    m_camera->getCameraDriveMode(_driveMode, true, &_driveModeList);
    ui->drivemodeLineEdit->setText(_driveMode);
    for(int i = 0; i < _driveModeList.size(); i++)
        ui->setDrivemodeComboBox->addItem(_driveModeList.at(i),i);
    ui->setDrivemodeComboBox->setCurrentText(_driveMode);
}

void CosmoppApp::initFocus()
{
    QString _focusMode = "";
    QVector<QString> _focusModeList;
    m_camera->getCameraFocus(_focusMode, true, &_focusModeList);
    ui->focusLineEdit->setText(_focusMode);
    for(int i = 0; i < _focusModeList.size(); i++)
        ui->setFocusComboBox->addItem(_focusModeList.at(i),i);
    ui->setFocusComboBox->setCurrentText(_focusMode);
}

void CosmoppApp::initWb()
{
    QString _wb = "";
    QVector<QString> _wbList;
    m_camera->getCameraWB(_wb, true, &_wbList);
    ui->wbLineEdit->setText(_wb);
    for(int i = 0; i < _wbList.size(); i++)
        ui->setWbComboBox->addItem(_wbList.at(i),i);
    ui->setWbComboBox->setCurrentText(_wb);
}

void CosmoppApp::initColorSpace()
{
    QString _colorSpace = "";
    QVector<QString> _colorSpaceList;
    m_camera->getCameraColorSpace(_colorSpace, true, &_colorSpaceList);
    ui->colorspaceLineEdit->setText(_colorSpace);
    for(int i = 0; i < _colorSpaceList.size(); i++)
        ui->setColorspaceComboBox->addItem(_colorSpaceList.at(i),i);
    ui->setColorspaceComboBox->setCurrentText(_colorSpace);
}

void CosmoppApp::initImageFormat()
{
    QString _imageFormat = "";
    QVector<QString> _imageFormatList;
    m_camera->getCameraImageFormat(_imageFormat, true, &_imageFormatList);
    ui->imgformatLineEdit->setText(_imageFormat);
    for(int i = 0; i < _imageFormatList.size(); i++)
        ui->setImgformatComboBox->addItem(_imageFormatList.at(i),i);
    ui->setImgformatComboBox->setCurrentText(_imageFormat);
}

void CosmoppApp::initSequenceTab()
{
    int _sec = 0, _min = 0, _exposure = 0;
    QString _exposureMode = "";
    ui->imageSequence->fitInView(m_sequence, Qt::KeepAspectRatio);
    ui->seqImageFormatLineEdit->setText(ui->imgformatLineEdit->text());
    ui->seqIsoLineEdit->setText(ui->isoLineEdit->text());
    ui->seqApertureLineEdit->setText(ui->apertureLineEdit->text());
    _exposureMode = ui->exposureLineEdit->text();
    ui->seqExposureLineEdit->setText(_exposureMode);
    if(_exposureMode.compare("bulb")==0)
    {
        _sec = ui->secSpinBox->value();
        _min = ui->minSpinBox->value();
        _exposure = _sec + _min*60;
        ui->seqSecLineEdit->setText(QString::number(_exposure));
    }
    else
    {
        ui->seqSecLineEdit->setText(_exposureMode);
    }
}

void CosmoppApp::startCapture()
{
    QString _exposure = "";
    QString _suffixPath = "";
    QString _index = "";
    m_imageFileName = ui->prefixLineEdit->text();
    m_camera->getCameraExposure(_exposure);
    bool _longExposureEnabled = false;
    _longExposureEnabled = _exposure.compare("bulb") == 0 ? true : false;
    if(_longExposureEnabled)
    {
        int _min = 0, _sec = 0, _exposureTime = 0;
        _min = ui->minSpinBox->value();
        _sec = ui->secSpinBox->value();
        _exposureTime = _min * 60 + _sec;
        if(m_sequenceEnabled)
        {
            qDebug() << "sequence start";
            m_imageIndex = ui->seqCurrentFrameLineEdit->text().toInt();
            _index = QString("%1").arg(m_imageIndex, IMG_INDEX_DIGITS, 10, QChar('0'));
            if(!ui->draftCheckBox->isChecked())
            {
                if(ui->seqLightButton->isChecked())
                {
                    _suffixPath = "light/";
                    m_imageType = COSMOPP_LIGHT_FRAME;
                }
                else if(ui->seqBiasButton->isChecked())
                {
                    _suffixPath = "bias/";
                    m_imageType = COSMOPP_BIAS_FRAME;
                }
                else if(ui->seqDarkButton->isChecked())
                {
                    _suffixPath = "dark/";
                    m_imageType = COSMOPP_DARK_FRAME;
                }
                else
                {
                    _suffixPath = "flat/";
                    m_imageType = COSMOPP_FLAT_FRAME;
                }
            }
            m_imagePath = m_imageDir + _suffixPath + m_imageFileName + "_" + _index + m_imageFileFormat;
            qDebug() << "acquiring image:" << m_imagePath;
            m_camera->setCaptureParameters(m_imagePath,true,_exposureTime);
            m_captureThread->start();
        }
        else
        {
            qDebug() << "normal capture";
            m_imageIndex = ui->frameNumLineEdit->text().toInt();
            _index = QString("%1").arg(m_imageIndex, IMG_INDEX_DIGITS, 10, QChar('0'));
            m_imagePath = m_imageDir + m_imageFileName + "_" + _index + m_imageFileFormat;
            qDebug() << "acquiring image:" << m_imagePath;
            m_camera->setCaptureParameters(m_imagePath,true,_exposureTime);
            m_captureThread->start();
        }
    }
    else
    {
        if(m_sequenceEnabled)
        {
            qDebug() << "sequence start";
            m_imageIndex = ui->seqCurrentFrameLineEdit->text().toInt();
            _index = QString("%1").arg(m_imageIndex, IMG_INDEX_DIGITS, 10, QChar('0'));
            if(!ui->draftCheckBox->isChecked())
            {
                if(ui->seqLightButton->isChecked())
                {
                    _suffixPath = "light/";
                    m_imageType = COSMOPP_LIGHT_FRAME;
                }
                else if(ui->seqBiasButton->isChecked())
                {
                    _suffixPath = "bias/";
                    m_imageType = COSMOPP_BIAS_FRAME;
                }
                else if(ui->seqDarkButton->isChecked())
                {
                    _suffixPath = "dark/";
                    m_imageType = COSMOPP_DARK_FRAME;
                }
                else
                {
                    _suffixPath = "flat/";
                    m_imageType = COSMOPP_FLAT_FRAME;
                }
            }
            m_imagePath = m_imageDir + _suffixPath + m_imageFileName + "_" + _index + m_imageFileFormat;
            qDebug() << "acquiring image:" << m_imagePath;
            m_camera->setCaptureParameters(m_imagePath);
            m_captureThread->start();
        }
        else
        {
            qDebug() << "normal capture";
            m_imageType = COSMOPP_NOINFO_FRAME;
            m_imageIndex = ui->frameNumLineEdit->text().toInt();
            _index = QString("%1").arg(m_imageIndex, IMG_INDEX_DIGITS, 10, QChar('0'));
            m_imagePath = m_imageDir + m_imageFileName + "_" + _index + m_imageFileFormat;
            qDebug() << "acquiring image:" << m_imagePath;
            m_camera->setCaptureParameters(m_imagePath);
            m_captureThread->start();
        }
    }
}

void CosmoppApp::updateBattery()
{
    checkBattery();
}

void CosmoppApp::on_updateFocusPushButton_pressed()
{
    initFocus();
}

void CosmoppApp::on_startCapturePushButton_pressed()
{
    startCapture();
}

void CosmoppApp::on_stopCapturePushButton_pressed()
{
    m_camera->stopCaptureImage();
}

void CosmoppApp::on_showImageAcquired(QString path)
{
    qDebug() << "image acquired in:" << path;
    QString _fileName = "";
    QString _index = "";
    _index = QString("%1").arg(m_imageIndex, IMG_INDEX_DIGITS, 10, QChar('0'));
    _fileName = m_imageFileName + "_" + _index;
    m_imageIndex++;
    //imagePreview
    QString _imagePath2 = path + ".jpg";
    QImage _image;
    _image.load(_imagePath2);
    updateImageFileInfo(_imagePath2);
    AstroImageInfo _astroImgInfo;
    _astroImgInfo.imageType = m_imageType;
    _astroImgInfo.iso = ui->isoLineEdit->text();
    _astroImgInfo.aperture = ui->apertureLineEdit->text();
    _astroImgInfo.format = ui->imgformatLineEdit->text();
    QString _exposureString = "";
    _exposureString = ui->exposureLineEdit->text();
    bool _longExposureEnabled = false;
    _longExposureEnabled = _exposureString.compare("bulb") == 0 ? true : false;
    _astroImgInfo.longExposure = _longExposureEnabled;
    if(_longExposureEnabled)
    {
       int _min = 0, _sec = 0, _tmpExposureTime = 0;
        _min = ui->minSpinBox->value();
        _sec = ui->secSpinBox->value();
        _tmpExposureTime = _min * 60 + _sec;
        _astroImgInfo.exposureTime = QString::number(_tmpExposureTime);
    }
    else
    {
        _astroImgInfo.exposureTime = _exposureString;
    }
    _astroImgInfo.index = m_imageIndex;
    _astroImgInfo.imagePath = _imagePath2;
    emit newImageAcquired(_astroImgInfo);
    emit updateImageHisto(_imagePath2, false);
    if(m_sequenceEnabled)
    {
        qDebug() << "sequence image acquired";
        m_currentSequenceImagePath = _imagePath2;
        ui->seqImageFileName->setText(_fileName);
        ui->seqCurrentFrameLineEdit->setText(QString::number(m_imageIndex));
        m_sequence_scene.clear(); //DEBUG
        ui->imageSequence->setScene(&m_sequence_scene);
        m_sequence = m_sequence_scene.addPixmap(QPixmap::fromImage(_image));
        ui->imageSequence->fitInView(m_sequence,Qt::KeepAspectRatio); 
    }
    else
    {
        qDebug() << "preview image acquired";
        m_currentPreviewImagePath = _imagePath2;
        ui->imgPathLineEdit->setText(_fileName);
        ui->frameNumLineEdit->setText(QString::number(m_imageIndex));
        m_preview_scene.clear(); //DEBUG
        ui->imagePreview->setScene(&m_preview_scene);
        m_preview = m_preview_scene.addPixmap(QPixmap::fromImage(_image));
        ui->imagePreview->fitInView(m_preview,Qt::KeepAspectRatio);
    }
}

void CosmoppApp::on_selectFolderButton_pressed()
{
    QString _homePath = QDir::homePath();
    QString _dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), _homePath, QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(_dir.isEmpty()) _dir = QDir::homePath();
    _dir = _dir + "/";
    if(m_imageDir.compare(_dir) != 0)
    {
        m_imageDir = _dir;
        if(ui->draftCheckBox->isChecked())
        {
            m_imageDir = m_imageDir + "draft/";
        }
    }
    qDebug() << "image dir:" << m_imageDir;
    m_camera->initCamera();
}

void CosmoppApp::on_draftCheckBox_stateChanged(int arg1)
{
    std::string _imageDir = "";
    if(arg1 == 2)
    {
        m_imageDir = m_imageDir + "draft/";
    }
    else if(arg1 == 0)
    {
        _imageDir = m_imageDir.toStdString();
        for(int i = 0; i < 6; i++)
            _imageDir.pop_back();
        m_imageDir = QString::fromStdString(_imageDir);
    }
}

void CosmoppApp::on_cameraTabWidget_currentChanged(int index)
{
    qDebug() << "change tab to:" << index;
    if(index == 1)
    {
        initSequenceTab();
        m_imageIndex = ui->seqCurrentFrameLineEdit->text().toInt();
        m_isSequenceTab = true;
        updateImageFileInfo(m_currentSequenceImagePath);
        if(m_imageHisto != nullptr)
            m_imageHisto->hide();
    }
    else if(index == 0)
    {
        m_imageIndex = ui->frameNumLineEdit->text().toInt();
        m_isSequenceTab = false;
        updateImageFileInfo(m_currentPreviewImagePath);
        if(m_imageHisto != nullptr)
            m_imageHisto->hide();
    }
}

void CosmoppApp::on_seqStartButton_pressed()
{
    int _currentNumber = 0;
    delay_MSec(ui->seqDelayLineEdit->text().toInt()*1000);
    _currentNumber = ui->seqCurrentFrameLineEdit->text().toInt();
    emit exeSequence(_currentNumber);
}

void CosmoppApp::on_seqStopButton_pressed()
{
    m_camera->stopCaptureImage();
    m_sequenceEnabled = false;
}

void CosmoppApp::on_exeSequence(int frame)
{
    int _maxFrameNumber = 0;
    int _frameNumber = 0;
    _maxFrameNumber = ui->seqMaxFrameLineEdit->text().toInt();
    _frameNumber = _maxFrameNumber - frame + 1;
    qDebug() << "frameNumber:" << _frameNumber;
    if(_frameNumber != 0)
    {
        m_sequenceEnabled = true;
        startCapture();
    }
    else
    {
        m_sequenceEnabled = false;
        ui->seqCurrentFrameLineEdit->setText("0");
    }
}

void CosmoppApp::on_seqCaptureThreadFinished()
{
    if(m_sequenceEnabled)
    {
        //on_seqStartButton_pressed();
        int _currentNumber = 0;
        _currentNumber = ui->seqCurrentFrameLineEdit->text().toInt();
        delay_MSec(ui->seqIntervalLineEdit->text().toInt()*1000);
        emit exeSequence(_currentNumber);
    }
}

void CosmoppApp::delay_MSec(unsigned int msec)
{
    QEventLoop loop;
    QTimer::singleShot(msec, &loop, SLOT(quit()));
    loop.exec();
}

void CosmoppApp::on_imageInfoToolButton_pressed()
{
    m_imageInfo->show();
}

void CosmoppApp::on_seqImageInfoToolButton_pressed()
{
    m_imageInfo->show();
}

void CosmoppApp::updateImageFileInfo(const QString path)
{
    QFileInfo _fileInfo(path);
    QString _info = "";
    _info += QString("<b>File name: </b>%1.%2<br/>").arg(_fileInfo.completeBaseName()).arg(_fileInfo.suffix());
    _info += QString("<b>File size: </b>%1 byte<br/>").arg(_fileInfo.size());
    _info += QString("<b>Date: </b>%1<br/>").arg(_fileInfo.lastModified().toString());
    emit updateImageInfo(_info);
}

void CosmoppApp::on_histoToolButton_pressed()
{
    emit updateImageHistoTool(m_currentPreviewImagePath,false);
    m_imageHisto->show();
}

void CosmoppApp::on_seqHistoToolButton_pressed()
{
    emit updateImageHistoTool(m_currentSequenceImagePath,false);
    m_imageHisto->show();
}
