QT       += core gui
QT       += charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/biasframe.cpp \
    src/darkframe.cpp \
    src/flatframe.cpp \
    src/lightframe.cpp \
    src/imagehisto.cpp \
    src/histo.cpp \
    src/imageinfo.cpp \
    src/gcamera.cpp \
    src/main.cpp \
    src/cosmoppapp.cpp

HEADERS += \
    include/biasframe.h \
    include/darkframe.h \
    include/flatframe.h \
    include/common.hpp \
    include/imagehisto.h \
    include/histo.hpp \
    include/imageinfo.h \
    include/camera.hpp \
    include/cosmoppapp.h \
    include/gcamera.hpp \
    include/gcameraerror.hpp \
    include/lightframe.h

FORMS += \
    forms/biasframe.ui \
    forms/darkframe.ui \
    forms/flatframe.ui \
    forms/cosmoppapp.ui \
    forms/imageinfo.ui \
    forms/imagehisto.ui \
    forms/lightframe.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc
